<?php
    require_once __DIR__ . "/inc/bootstrap.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href= "login.css">
    <!-- Inter Font -->
    <link rel="preconnect" href="https://rsms.me/">
    <link rel="stylesheet" href="https://rsms.me/inter/inter.css">
    <!-- DM Serif & DM Sans Font-->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=DM+Sans&family=DM+Serif+Display&display=swap" rel="stylesheet">
    <title>Log in</title>
</head>

<body>
    <div class="card">
        <div class="card-header">
            <h2>Welcome back!</h2>
            Please enter your details.
        </div>
        <form id="login-form">
            <div>
                <input type="text" id="username-input" name="username" placeholder="Enter your username">
            </div>
            <div>
                <input type="password" name="password" id="password-input" placeholder="Enter your password">
            </div>
            <div class="row g-0">
                <button type="login-button">Log in</button>
            </div>
        </form>
        <p class="error-message display-none"></p>
    </div>
    <!-- JQuery  -->
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"
        integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(() => {
            $('#login-form').submit((e) => {
                e.preventDefault();
                const username = $('#username-input').val();
                const password = $('#password-input').val();
                $.ajax({
                    url: "/pvi-labs/Controller/LoginController",
                    type: "POST",
                    data: {
                        username: username,
                        password: password,
                    },
                    dataType: "json",
                }).done((data) => {
                    console.log(data);
                    if (data.status == "success") {
                        window.location.href = "/pvi-labs/";
                    } else {
                        $('.error-message').text(data.errors).removeClass('display-none');
                        $('input').trigger('blur');
                        $('input').addClass('red-border');
                    }
                })
                .fail((data) => {
                    console.log(data);
                })
            })

            $('input').focus(() => $('input').removeClass('red-border'));
        })
    </script>
</body>
</html>