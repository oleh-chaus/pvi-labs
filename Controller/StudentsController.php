<?php
require_once __DIR__ . "/../Model/StudentsModel.php";
try {
    $students_model = new StudentsModel();
} catch (Exception $e) {
    $response = array("status" => "error", "errors" => "Error connecting to database.");
    echo json_encode($response);
    return;
}

function validateInput($student) {
    $errors = array();
    foreach ($student as $key => $value) {
        if (empty($value)) {
            array_push($errors, "Please enter <span class=\"text-bg-danger fw-bold\">{$key}</span><br>");
            $valid = false;
        }
        else {
            if ($key == "name" || $key == "surname") {
                if (!preg_match("/^[\p{L}\-\p{Zs}]+$/", $value)) {
                    array_push($errors, "The <span class=\"text-bg-danger fw-bold\">{$key}</span> <span class=\"text-bg-warning fw-bold\">\"{$value}\"</span> is invalid.<br>");
                    $valid = false;
                }
            }
            if ($key == "birthday") {
                $birthday = strtotime($value);
                if ((time() - $birthday) < (16 * 31536000)) {
                    array_push($errors, "The age is invalid.<br>");
                    $valid = false;
                }
            }
        }
    }
    return $errors;
}

function addStudent($student, $students_model) {
    $group = $student['group'];
    $name = $student['name'];
    $surname = $student['surname'];
    $gender = $student['gender'];
    $birthday = $student['birthday'];
    $result = $students_model->insertStudent($group, $name, $surname, $gender, $birthday);
    if($result) {
        $response = array("status" => "success");
        echo json_encode($response);
    }
    else {
        $response = array("status" => "error", "errors" => mysqli_error($connection));
        echo json_encode($response);
    }
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $student = $_POST;
    $errors = validateInput($student);
    if(empty($errors)) {
        addStudent($student, $students_model);
    }
    else {
        $response = array("status" => "error", "errors" => $errors);
        echo json_encode($response);
    }
}
if($_SERVER["REQUEST_METHOD"] == "GET") {
    echo json_encode(array("status" => "success", "students" => $students_model->getStudents()));
}
if($_SERVER["REQUEST_METHOD"] == "PUT") {
    parse_str(file_get_contents("php://input"), $student);
    $group = $student['group'];
    $name = $student['name'];
    $surname = $student['surname'];
    $gender = $student['gender'];
    $birthday = $student['birthday'];
    $id = $student['id'];
    $errors = validateInput($student);
    if(empty($errors)) {
        $students_model->updateStudent($id, $group, $name, $surname, $gender, $birthday);
        $response = array("status" => "success");
        echo json_encode($response);
    }
    else {
        $response = array("status" => "error", "errors" => $errors);
        echo json_encode($response);
    }
}
if($_SERVER["REQUEST_METHOD"] == "DELETE") {
    parse_str(file_get_contents("php://input"), $student);
    $id = $student['id'];
    $students_model->removeStudent($id);
    $response = array("status" => "success");
    echo json_encode($response);
}
?>