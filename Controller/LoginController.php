 <?php
require_once __DIR__ . "/../Model/UserModel.php";
try {
    $user_model = new UserModel();
} catch (Exception $e) {
    $response = array("status" => "error", "errors" => "Error connecting to database.");
    echo json_encode($response);
    return;
}

function login($username, $password, $user_model) {
    $user = $user_model->findUserByUsernameAndPassword($username, $password);

    if ($user) {
        session_start();
        $_SESSION['loggedin'] = TRUE;
        $_SESSION['name'] = $user['username'];
        $_SESSION['id'] = $user['id'];
        $response = array("status" => "success");
        echo json_encode($response);
    } else {
        $response = array("status" => "error", "errors" => "Incorrect username or password.");
        echo json_encode($response);
    }
}
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['username']) && isset($_POST['password'])) {
        // Get the username and password from the form
        $username = $_POST['username'];
        $password = $_POST['password'];
        if(empty($username) || empty($password)) {
            $response = array("status" => "error", "errors" => "Please enter username and password.");
            echo json_encode($response);
            return;
        }
        login($username, $password, $user_model);
    }
    else {
        $response = array("status" => "error", "errors" => "No password or username provided.");
        echo json_encode($response);
    }
}
