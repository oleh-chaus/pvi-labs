<?php
//TODO: Add a profile page for the user

require_once( __DIR__ . "/inc/config.php");
require_once(__DIR__ . "/Model/UserModel.php");

$user_model = new UserModel();

session_start();

if(isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true){
    header("Location: students.php");
}
else{
    header("Location: login.php");
}