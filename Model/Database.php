<?php
require_once  __DIR__ . "/../inc/config.php";
class Database {
    protected $connection = null;

    public function __construct() {
        try {
            $this->connection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE_NAME);
            if (mysqli_connect_errno()) {
                throw new Exception("Could not connect to database.");   
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function select($query = "", $params = []) {
        try {
            $stmt = $this->executeStatement($query, $params);
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            $stmt->close();

            return $result;
        }
        catch(Exception $e) {
            throw new Exception($e->getMessage());   
        }
        return false;
    }

    public function insert($query = "", $params = []) {
        try {
            $stmt = $this->executeStatement($query, $params);
            $stmt->close();
        }
        catch(Exception $e) {
            throw new Exception($e->getMessage());   
        }
    }

    public function delete($query = "", $params = []) {
        try {
            $stmt = $this->executeStatement($query, $params);
            $stmt->close();
        }
        catch(Exception $e) {
            throw new Exception($e->getMessage());   
        }
    }

    private function executeStatement($query = "", $params = []) {
        try {
            $stmt = $this->connection->prepare($query);
            
            if(!$stmt) {
                throw new Exception("Could not prepare statement.");   
            }

            if (count($params) > 0) {
                $values = $params[0];
                $params = array_slice($params, 1);
                $stmt->bind_param($values, ...$params);
            }
            $stmt->execute();
            return $stmt;
        }
        catch(Exception $e) {
            throw new Exception($e->getMessage());   
        }
    }
}
?>