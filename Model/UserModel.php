<?php
require_once "Database.php";

class UserModel extends Database {
    public function findUserByUsernameAndPassword($username = "", $password = "") {
        try {
            $query = "SELECT * FROM accounts WHERE username = ? LIMIT 1";
            $params = ["s", $username];
            $result = $this->select($query, $params);
            if (count($result) && password_verify($password, $result[0]["password"])) {
                return $result[0];
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
}

?>