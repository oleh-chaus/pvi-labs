<?php
require_once "Database.php";

class StudentsModel extends Database {
    public function insertStudent($group, $name, $surname, $gender, $birthday) {
        try {
            $query = "INSERT INTO `students_list` (`group`, `name`, `surname`, `gender`, `birthday`) VALUES (?, ?, ?, ?, ?)";
            $params = ["sssss", $group, $name, $surname, $gender, $birthday];
            $this->insert($query, $params);
            return true;
        }
        catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }

    public function getStudents() {
        $query = "SELECT * FROM `students_list`";
        $result = $this->select($query);
        return $result;
    }

    public function removeStudent($id) {
        $query = "DELETE FROM `students_list` WHERE `id` = ?";
        $params = ["i", $id];
        $this->insert($query, $params);
    }

    public function updateStudent($id, $group, $name, $surname, $gender, $birthday) {
        try {
            $query = "UPDATE `students_list` SET 
            `group` = ?,
            `name` = ?,
            `surname` = ?,
            `gender` = ?,
            `birthday` = ?
            WHERE `id` = ?";
            $params = ["sssssi", $group, $name, $surname, $gender, $birthday, $id];
            $this->insert($query, $params);
            return true;
        }
        catch(Exception $e) {
            throw new Exception($e->getMessage());
        }
        return false;
    }
}

?>